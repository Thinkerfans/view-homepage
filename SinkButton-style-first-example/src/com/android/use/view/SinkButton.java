package com.android.use.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;

public class SinkButton extends RelativeLayout {
	private static final String TAG = "SinkButton";

	private static final float PRESS_SCALE = 0.95f;
	private boolean mMoveOut = false;

	private Animation mPressAnim;
	private Animation mReleaseAnim;

	private static float sMoveMax = 10;

	public SinkButton(Context context) {
		super(context);
		init();
	}

	public SinkButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		setClickable(true);
		DisplayMetrics dm = getResources().getDisplayMetrics();
		sMoveMax = 10 * dm.density;
	}

	private Animation getPressAnimation() {
		if (mPressAnim == null) {
			mPressAnim = new ScaleAnimation(1, PRESS_SCALE, 1, PRESS_SCALE, getWidth() / 2f, getHeight() / 2f);
			mPressAnim.setDuration(200);
			mPressAnim.setFillAfter(true);
		}
		return mPressAnim;
	}

	private Animation getReleaseAnimation() {
		if (mReleaseAnim == null) {
			mReleaseAnim = new ScaleAnimation(PRESS_SCALE, 1, PRESS_SCALE, 1, getWidth() / 2f, getHeight() / 2f);
			mReleaseAnim.setDuration(200);
		}
		return mReleaseAnim;
	}

	private float mRawX, mRawY;

	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
//			Log.d(TAG, "ACTION_DOWN");
			mRawX = event.getX();
			mRawY = event.getY();

			startAnimation(getPressAnimation());
			mMoveOut = false;
			break;
		case MotionEvent.ACTION_MOVE:
			Log.d(TAG, "ACTION_MOVE:" + (int) event.getX() + "," + (int) event.getY());
			if (Math.abs(event.getX() - mRawX) > sMoveMax || Math.abs(event.getY() - mRawY) > sMoveMax) {
				mMoveOut = true;
				Log.d(TAG, "ACTION_MOVE:release");
				clearAnimation();
			}
			break;
		case MotionEvent.ACTION_CANCEL:
			clearAnimation();
			break;
		case MotionEvent.ACTION_UP:
			clearAnimation();
//			Log.d(TAG, "ACTION_UP");
			if (!mMoveOut) {
				Log.d(TAG, "click happened!");
				Animation a = getReleaseAnimation();
				startAnimation(a);
				a.setAnimationListener(new Animation.AnimationListener() {
					@Override
					public void onAnimationStart(Animation animation) {
					}

					@Override
					public void onAnimationRepeat(Animation animation) {
					}

					@Override
					public void onAnimationEnd(Animation animation) {
						performClick();
					}
				});
				startAnimation(a);
			}
			return true;
		}
		return super.onInterceptTouchEvent(event);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
//			Log.d(TAG, "ACTION_DOWN");
			mRawX = event.getX();
			mRawY = event.getY();

			startAnimation(getPressAnimation());
			mMoveOut = false;
			break;
		case MotionEvent.ACTION_MOVE:
//			Log.d(TAG, "ACTION_MOVE:" + (int) event.getX() + "," + (int) event.getY());
			if (Math.abs(event.getX() - mRawX) > sMoveMax || Math.abs(event.getY() - mRawY) > sMoveMax) {
				mMoveOut = true;
				Log.d(TAG, "ACTION_MOVE:release");
				clearAnimation();
			}
			break;
		case MotionEvent.ACTION_CANCEL:
			clearAnimation();
			break;
		case MotionEvent.ACTION_UP:
			clearAnimation();
//			Log.d(TAG, "ACTION_UP");
			if (!mMoveOut) {
				Log.d(TAG, "click happened!");
				Animation a = getReleaseAnimation();
				startAnimation(a);
				a.setAnimationListener(new Animation.AnimationListener() {
					@Override
					public void onAnimationStart(Animation animation) {
					}

					@Override
					public void onAnimationRepeat(Animation animation) {
					}

					@Override
					public void onAnimationEnd(Animation animation) {
						performClick();
					}
				});
				startAnimation(a);
			}
			return true;
		}
		return super.onTouchEvent(event);
	}
}
