package com.example.sinkbutton;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class SinkButtonTestActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.home_layout);
		initView();
		
	}

	private void initView() {
		findViewById(R.id.home_information).setOnClickListener(this);
		findViewById(R.id.home_query_wz).setOnClickListener(this);
		findViewById(R.id.home_bbs).setOnClickListener(this);
		findViewById(R.id.home_carbrand).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.home_information:
			Toast.makeText(this, "information", Toast.LENGTH_SHORT).show();
			break;
		case R.id.home_bbs:
			Toast.makeText(this, "bbs", Toast.LENGTH_SHORT).show();
			break;
		case R.id.home_query_wz:
			Toast.makeText(this, "wz", Toast.LENGTH_SHORT).show();
			break;
		case R.id.home_carbrand:
			Toast.makeText(this, "carbrand", Toast.LENGTH_SHORT).show();
			break;
		default:
			break;
		}
	}

}
